
# We hardcode the version numbers since we cannot determine versions during
# configure stage.
set (cmb_version_major 4)
set (cmb_version_minor 0)
set (cmb_version_patch 0)
set (cmb_version_suffix)
set (cmb_version "${cmb_version_major}.${cmb_version_minor}")
