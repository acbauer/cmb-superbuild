
add_external_project_or_just_build_dependencies(smtk
  DEPENDS boost qt shiboken paraview remus
  CMAKE_ARGS
    ${extra_cmake_args}
    "-C${CMAKE_BINARY_DIR}/env.cmake"
    -DBUILD_SHARED_LIBS:BOOL=ON

    -DSMTK_BUILD_QT:BOOL=ON
    -DSMTK_BUILD_VTK:BOOL=ON
    -DSMTK_BUILD_DISCRETE_SESSION:BOOL=ON
    -DSMTK_BUILD_SESSION_PLUGIN:BOOL=ON
    -DSMTK_ENABLE_EXODUS_SESSION:BOOL=ON
    -DSMTK_ENABLE_REMUS:BOOL=ON

    -DParaView_DIR:PATH=${SuperBuild_BINARY_DIR}/paraview/src/paraview-build/
    -DSMTK_BUILD_PYTHON_WRAPPINGS:BOOL=${shiboken_ENABLED}
    -DBOOST_INCLUDEDIR:PATH=<INSTALL_DIR>/include/boost
    -DBOOST_LIBRARYDIR:PATH=<INSTALL_DIR>/lib
  )

if(shiboken_ENABLED)
  add_external_project_step(install_shiboken_python_plugin
    COMMENT "Fixing missing include files."
    COMMAND  ${CMAKE_COMMAND}
      -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
      -Dpv_version:STRING=${pv_version}
      -Dpv_python_executable:PATH=${pv_python_executable}
      -DBUILD_SHARED_LIBS:BOOL=ON
      -DSMTK_BIN_DIR:PATH=${install_location}
      -DTMP_DIR:PATH=<TMP_DIR>
      -P ${CMAKE_CURRENT_LIST_DIR}/install_smtk_python_plugin.cmake
    DEPENDEES install)
endif()

