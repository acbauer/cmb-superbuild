option(SUPPRESS_HDF5_BUILD_OUTPUT
       "Suppress HDF5 build output"
      ON)
mark_as_advanced(SUPPRESS_HDF5_BUILD_OUTPUT)

set(suppress_build_out)

if(SUPPRESS_HDF5_BUILD_OUTPUT)
  set(suppress_build_out SUPPRESS_BUILD_OUTPUT)
endif()

add_external_project(
  hdf5
  DEPENDS zlib szip

  #always build in release mode.
  #enable install name so that we get full paths to library on apple
  CMAKE_ARGS
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DHDF5_ENABLE_Z_LIB_SUPPORT:BOOL=TRUE
    -DHDF5_ENABLE_SZIP_SUPPORT:BOOL=TRUE
    -DHDF5_ENABLE_SZIP_ENCODING:BOOL=TRUE
    -DHDF5_BUILD_HL_LIB:BOOL=TRUE
    -DHDF5_BUILD_WITH_INSTALL_NAME:BOOL=TRUE
  ${suppress_build_out}
)
