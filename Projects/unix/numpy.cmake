option(SUPPRESS_NUMPY_BUILD_OUTPUT
       "Suppress numpy build output"
      ON)
mark_as_advanced(SUPPRESS_NUMPY_BUILD_OUTPUT)

set(suppress_build_out)

if(SUPPRESS_NUMPY_BUILD_OUTPUT)
  set(suppress_build_out SUPPRESS_BUILD_OUTPUT)
endif()


add_external_project_or_use_system(numpy
  DEPENDS python
  CONFIGURE_COMMAND ""
  INSTALL_COMMAND ""
  BUILD_IN_SOURCE 1
  ${suppress_build_out}
  BUILD_COMMAND
    ${pv_python_executable} setup.py install --prefix=<INSTALL_DIR>
)
