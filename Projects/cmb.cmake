
add_external_project_or_just_build_dependencies(cmb
  DEPENDS boost remus vxl kml gdal qt python paraview molequeue smtk
  DEPENDS_OPTIONAL moab
  CMAKE_ARGS
    ${extra_cmake_args}
    -DKML_DIR:PATH=<INSTALL_DIR>
    -DGDAL_DIR:PATH=<INSTALL_DIR>
    -DParaView_DIR:PATH=${SuperBuild_BINARY_DIR}/paraview/src/paraview-build
    -DVTK_DIR:PATH=${SuperBuild_BINARY_DIR}/paraview/src/paraview-build/VTK
    -DMoleQueue_DIR:PATH=<INSTALL_DIR>

    #specify if we should build meshing workers
    -DBUILD_MESH_WORKERS:BOOL=${USE_MESHING_SUPPORT}

    # specify the apple app install prefix. No harm in specifying it for all
    # platforms.
    -DMACOSX_APP_INSTALL_PREFIX:PATH=<INSTALL_DIR>/Applications
)

#special mac only script to install plugin for paraview
if(APPLE)
  include(cmb_version)
  add_external_project_step(install_cmb_paraview_plugin
    COMMENT "Fixing missing include files."
    COMMAND  ${CMAKE_COMMAND}
      -DBUILD_SHARED_LIBS:BOOL=ON
      -DINSTALL_DIR:PATH=<INSTALL_DIR>
      -DCMB_BINARY_DIR:PATH=${SuperBuild_BINARY_DIR}/cmb/src/cmb-build
      -DTMP_DIR:PATH=<TMP_DIR>
      -DCMB_VERSION:STRING=${cmb_version}
      -P ${CMAKE_CURRENT_LIST_DIR}/apple/install_cmb_paraview_plugin.cmake
    DEPENDEES install)

  #special mac only script to install smtk plugins for cmb
  add_external_project_step(install_smtk_cmb_plugins
    COMMENT "installing smtk plugins for cmb."
    COMMAND  ${CMAKE_COMMAND}
      -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
      -DBUILD_SHARED_LIBS:BOOL=ON
      -DINSTALL_DIR:PATH=<INSTALL_DIR>
      -DSMTK_BIN_DIR:PATH=${install_location}
      -DTMP_DIR:PATH=<TMP_DIR>
      -P ${CMAKE_CURRENT_LIST_DIR}/apple/install_smtk_cmb_plugin.cmake
    DEPENDEES install)

endif()
