if (APPLE)
  message(FATAL_ERROR "ABORT")
endif()

option(SUPPRESS_PYTHON_BUILD_OUTPUT
       "Suppress Python build output"
      ON)
mark_as_advanced(SUPPRESS_PYTHON_BUILD_OUTPUT)

set(suppress_build_out)

if(SUPPRESS_PYTHON_BUILD_OUTPUT)
  set(suppress_build_out SUPPRESS_BUILD_OUTPUT)
endif()

set(libtype "--enable-shared")
if (CROSS_BUILD_STAGE STREQUAL "TOOLS")
  set(libtype "--enable-static --disable-shared")
endif()

add_external_project_or_use_system(python
  DEPENDS zlib png
  CONFIGURE_COMMAND <SOURCE_DIR>/configure
                    --prefix=<INSTALL_DIR>
                    --enable-unicode
                    ${libtype}
  ${suppress_build_out}
  )
set (pv_python_executable "${install_location}/bin/python" CACHE INTERNAL "" FORCE)
