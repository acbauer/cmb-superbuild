option(SUPPRESS_KML_BUILD_OUTPUT
       "Suppress KML build output"
      ON)
mark_as_advanced(SUPPRESS_KML_BUILD_OUTPUT)

set(suppress_build_out)

if(SUPPRESS_KML_BUILD_OUTPUT)
  set(suppress_build_out SUPPRESS_BUILD_OUTPUT)
endif()

add_external_project(kml
  # kml has problems with newer gcc compilers with not including unistd.h
  PATCH_COMMAND ${CMAKE_COMMAND} -E copy_if_different
                ${SuperBuild_PROJECTS_DIR}/patches/kml.src.kml.base.file_posix.cc
                <SOURCE_DIR>/src/kml/base/file_posix.cc
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS=OFF
  ${suppress_build_out}
  )
