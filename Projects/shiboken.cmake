option(SUPPRESS_SHIBOKEN_BUILD_OUTPUT
       "Suppress SHIBOKEN build output"
      ON)
mark_as_advanced(SUPPRESS_SHIBOKEN_BUILD_OUTPUT)

set(suppress_build_out)

if(SUPPRESS_SHIBOKEN_BUILD_OUTPUT)
  set(suppress_build_out SUPPRESS_BUILD_OUTPUT)
endif()

add_external_project(shiboken
  DEPENDS qt python
  CMAKE_ARGS
    "-C${CMAKE_BINARY_DIR}/env.cmake"
    -DSET_RPATH:BOOL=ON
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DDISABLE_DOCSTRINGS:BOOL=ON
	-DBUILD_TESTS:BOOL=OFF
  ${suppress_build_out}
  )
