option(SUPPRESS_GDAL_BUILD_OUTPUT
       "Suppress GDAL build output"
      ON)
mark_as_advanced(SUPPRESS_GDAL_BUILD_OUTPUT)

set(suppress_build_out)

if(SUPPRESS_GDAL_BUILD_OUTPUT)
  set(suppress_build_out SUPPRESS_BUILD_OUTPUT)
endif()

add_external_project(gdal
  PATCH_COMMAND ${CMAKE_COMMAND} -E copy_if_different
                ${SuperBuild_PROJECTS_DIR}/patches/gdal.frmts.CMakeLists.txt
                <SOURCE_DIR>/frmts/CMakeLists.txt
  CMAKE_ARGS
  ${suppress_build_out}
)