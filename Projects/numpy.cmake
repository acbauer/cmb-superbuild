set (_install_location "<INSTALL_DIR>")
if (WIN32)
  # numpy build has issues with paths containing "C:". So we set the prefix as a
  # relative path.
  set (_install_location "../../../install")
endif()

option(SUPPRESS_NUMPY_BUILD_OUTPUT
       "Suppress numpy build output"
      ON)
mark_as_advanced(SUPPRESS_NUMPY_BUILD_OUTPUT)

set(suppress_build_out)

if(SUPPRESS_NUMPY_BUILD_OUTPUT)
  set(suppress_build_out SUPPRESS_BUILD_OUTPUT)
endif()

add_external_project(numpy
  DEPENDS python
  CONFIGURE_COMMAND ""
  INSTALL_COMMAND ""
  BUILD_IN_SOURCE 1
  ${suppress_build_out}
  SUPPRESS_BUILD_OUTPUT
  BUILD_COMMAND
    ${pv_python_executable} setup.py install --prefix=${_install_location}
)
