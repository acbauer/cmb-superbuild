option(SUPPRESS_MOLEQUEUE_BUILD_OUTPUT
       "Suppress Molequeue build output"
      ON)
mark_as_advanced(SUPPRESS_MOLEQUEUE_BUILD_OUTPUT)

set(suppress_build_out)

if(SUPPRESS_MOLEQUEUE_BUILD_OUTPUT)
  set(suppress_build_out SUPPRESS_BUILD_OUTPUT)
endif()

add_external_project(molequeue
  DEPENDS qt

  PATCH_COMMAND ${CMAKE_COMMAND} -E copy_if_different
                ${SuperBuild_PROJECTS_DIR}/patches/molequeue.transport.messageidmanager_p.cpp
                <SOURCE_DIR>/molequeue/transport/messageidmanager_p.cpp

  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DENABLE_TESTING:BOOL=OFF
    -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
  ${suppress_build_out}
)
