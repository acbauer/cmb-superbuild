
#We only have to do this special installation of the CMB libraries as a plugin
#for ParaView on MAC

set (SHARED_LIBRARY_SUFFIX ".dylib")
execute_process(COMMAND ${CMAKE_COMMAND} -E remove_directory ${TMP_DIR}/smtkCmbPlugin)
execute_process(COMMAND ${CMAKE_COMMAND} -E make_directory ${TMP_DIR}/smtkCmbPlugin)

set(plugin_install_dir ${INSTALL_DIR}/Applications/ModelBuilder.app/Contents/Plugins)
execute_process(COMMAND ${CMAKE_COMMAND} -E make_directory ${plugin_install_dir})

file(COPY "${INSTALL_DIR}/lib/"
     DESTINATION "${TMP_DIR}/smtkCmbPlugin"
     FILES_MATCHING
     PATTERN "libsmtk*_Plugin*${SHARED_LIBRARY_SUFFIX}"
    )

execute_process(
  COMMAND ${CMAKE_CURRENT_LIST_DIR}/fixup_cmb_plugin.py
          # The directory containing the plugin dylibs.
          ${TMP_DIR}/smtkCmbPlugin
          # fixup only the id of the plugins based on this string
          "@executable_path/../Plugins/"
          )

#okay the plugin is fixed up, now we need to install it into paraviews bundle
file(COPY "${TMP_DIR}/smtkCmbPlugin/"
     DESTINATION "${plugin_install_dir}"
     FILES_MATCHING
     PATTERN "*Plugin*${SHARED_LIBRARY_SUFFIX}"
    )
